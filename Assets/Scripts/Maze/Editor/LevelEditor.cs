using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LevelEditor : EditorWindow
{
    private static Color[] tileColors =
    {
        Color.black,//pill color
        Color.white,//wall color
        Color.blue,//PacBear color
        Color.green,//ghosts color
        Color.yellow,//honey color
    };
    [SerializeField] private LevelData myData;
    [SerializeField] private float tileSize = 20f;
    [SerializeField] private float leftPadding = 5f;
    [SerializeField] private float tilePadding = 3f;
    [SerializeField] private float gridStart = 160f;
    [SerializeField] private int selectedTile = 1;

    [SerializeField] private string filePath = "Assets/test.txt";

    [MenuItem("Window/LevelEditor")]
    private static void ShowWindow()
    {
        var window = GetWindow<LevelEditor>();
        window.titleContent = new GUIContent("Level");
        window.myData = new LevelData(MazeGenerator.GenerateV2(new IntVector2(7, 7), 2, 2));
        window.Show();
    }

    private void Awake()
    {

    }

    private void OnGUI()
    {
        GUILayout.Label("Level Editor");

        GUILayout.BeginHorizontal();
        myData.Width = EditorGUILayout.IntField(myData.Width);
        myData.Height = EditorGUILayout.IntField(myData.Height);
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Regenerate Level old"))
        {
            myData.Grid = MazeGenerator.Generate(new IntVector2(myData.Width, myData.Height));
        }

        if (GUILayout.Button("Regenerate Level"))
        {
            myData.Grid = MazeGenerator.GenerateV2(new IntVector2(myData.Width, myData.Height), 4, 4);
        }

        if (GUILayout.Button("Save Level"))
        {
            SaveLevel();
        }

        Event e = Event.current;


        for (int i = 0; i < tileColors.Length; i++)
        {
            Rect colorArray = new Rect
                (
                (tileSize + tilePadding * 2) * i + leftPadding,
                gridStart - 2 * tileSize,
                tileSize, tileSize
                );

            if (e.type == EventType.MouseDown && colorArray.Contains(e.mousePosition))
            {
                selectedTile = i;
            }

            Rect selectedTileRect = new Rect
                (
                (tileSize + tilePadding * 2) * selectedTile + leftPadding + 0.25f,
                gridStart - 2 * tileSize + 0.25f,
                tileSize * 0.5f, tileSize * 0.5f
                );
            EditorGUI.DrawRect(selectedTileRect, Color.red);

            EditorGUI.DrawRect(colorArray, tileColors[i]);
        }

        if (myData.Grid == null) return;

        for (int i = 0; i < myData.Grid.GetLength(0); i++)
        {
            for (int j = 0; j < myData.Grid.GetLength(1); j++)
            {
                Rect gridTile = new Rect
                (
                (tileSize + tilePadding) * i + leftPadding,
                (tileSize + tilePadding) * j + gridStart,
                tileSize, tileSize
                );

                if ((e.type == EventType.MouseDown || e.type == EventType.MouseDrag) && gridTile.Contains(e.mousePosition))
                {
                    myData.Grid[i, j] = selectedTile;
                    Repaint();
                }
                int objectIndex = myData.Grid[i, j];
                EditorGUI.DrawRect(gridTile, tileColors[objectIndex]);
            }
        }
    }
    private void SaveLevel()
    {
        string jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(myData);
        System.IO.File.WriteAllText(filePath, jsonData);
        AssetDatabase.Refresh();
    }
}



