using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct LevelData
{

    public int Width;
    public int Height;
    public int[,] Grid;

    public LevelData(int width, int height)
    {
        Width = width;
        Height = height;
        Grid = new int[Height, Width];
    }
    public LevelData(int[,] grid)
    {
        Width = grid.GetLength(0);
        Height = grid.GetLength(1);
        Grid = grid;
    }
}

