using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlsUIScreen : UIScreen
{
    protected override void OnEnable()
    {
        base.OnEnable();
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 1f;
    }

    public void ButtonBack()
    {
        UIScreenController.ShowScreen<MainMenuUIScreen>();
    }
}
