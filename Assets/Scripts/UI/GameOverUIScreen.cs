using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverUIScreen : UIScreen
{
    [SerializeField]
    private TMP_Text _text;
    protected override void OnEnable()
    {
        base.OnEnable();
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.None;
    }

    public void ButtonRestart()
    {
        UIScreenController.ShowScreen<GameplayUIScreen>();
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    public void ButtonQuit()
    {
        UIScreenController.ShowScreen<MainMenuUIScreen>();
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }
}
