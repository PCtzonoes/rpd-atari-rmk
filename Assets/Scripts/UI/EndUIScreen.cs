using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndUIScreen : UIScreen
{
    protected override void OnEnable()
    {
        base.OnEnable();

        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0f;
    }
    public void ButtonRestart()
    {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
        UIScreenController.ShowScreen<GameplayUIScreen>();
    }

    public void ButtonQuit()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
        UIScreenController.ShowScreen<MainMenuUIScreen>();
    }
}
