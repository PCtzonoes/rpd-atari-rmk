﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

/// <summary>
/// A simple camera follower class. It saves the offset from the
///  focus position when started, and preserves that offset when following the focus.
/// </summary>
public class CameraController : MonoBehaviour
{
    public Transform focus;
    //public float smoothTime = 2;

    //float offset;

    void Awake()
    {
        //offset = Vector2.Distance(focus.position, transform.position);
    }

    private void Update()
    {
        // Vector3 offsetVector = (transform.position - focus.position).normalized * offset;
        // offsetVector = offsetVector + focus.position;
        // offsetVector.z = transform.position.z;

        Vector3 newPosition = focus.position;
        newPosition.z = transform.position.z;
        transform.position = newPosition;

        // transform.position = Vector3.Lerp(transform.position, offsetVector, Time.deltaTime * smoothTime);
        // transform.position = pixelPerfectCamera.RoundToPixel(transform.position);
    }
}
