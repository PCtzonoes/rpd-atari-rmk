using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameEvent/Default")]
public class GameEvent : ScriptableObject
{
    protected HashSet<GameEventListener> _listeners = new HashSet<GameEventListener>();

    public virtual void Invoke()
    {
        foreach (var eventListener in _listeners)
        {
            eventListener.RaiseEvent();
        }
    }

    public virtual void Register(GameEventListener gameEventListener) => _listeners.Add(gameEventListener);
    public virtual void Remove(GameEventListener gameEventListener) => _listeners.Remove(gameEventListener);
}
