using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] int _levelToLoadNumber = 1;
    [SerializeField] bool _toLoadOnStartup = true;
    void Start()
    {
        if (_toLoadOnStartup)
            SceneManager.LoadScene(_levelToLoadNumber, LoadSceneMode.Single);
    }
}
