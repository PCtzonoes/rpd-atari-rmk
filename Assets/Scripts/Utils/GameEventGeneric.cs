using System.Collections.Generic;
using UnityEngine;

public abstract class GameEventGeneric<T> : ScriptableObject
{
    protected HashSet<GameEventListenerGeneric<T>> _listeners = new HashSet<GameEventListenerGeneric<T>>();

    public virtual void Invoke()
    {
        foreach (var eventListener in _listeners)
        {
            eventListener.RaiseEvent();
        }
    }

    public virtual void Register(GameEventListenerGeneric<T> gameEventListener) => _listeners.Add(gameEventListener);
    public virtual void Remove(GameEventListenerGeneric<T> gameEventListener) => _listeners.Remove(gameEventListener);
}
