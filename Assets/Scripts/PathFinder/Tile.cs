using System;
using System.Collections.Generic;
using UnityEngine;

public class Tile : IComparable<Tile>
{
    public readonly bool isWalkable;
    public readonly IntVector2 pos;
    public float value;

    public Tile previousTile;

    public Tile(int x, int y, bool isWalkable)
    {
        pos = new IntVector2(x, y);
        this.isWalkable = isWalkable;
        value = 0.5f;
        previousTile = null;
    }

    public int CompareTo(Tile other)
    {
        if (this.value < other.value)
        {
            return -1;
        }
        else if (this.value > other.value)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
