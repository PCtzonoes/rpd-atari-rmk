using System;
using System.Collections;
using System.Collections.Generic;
// using System.Diagnostics;
// using System.Threading;
using UnityEngine;

public static class PathFinder
{

    public enum PathType
    {
        Old, Astar
    }
    private static Tile[,] graph;

    private static Queue<Tile> frontier;

    public static void GeneretaGraph(int[,] world, int nonWalkableIndex)
    {
        graph = new Tile[world.GetLength(0), world.GetLength(1)];

        for (int i = 0; i < graph.GetLength(0); i++)
        {
            for (int j = 0; j < graph.GetLength(1); j++)
            {
                bool walkable = world[i, j] != nonWalkableIndex;
                graph[i, j] = new Tile(i, j, walkable);
            }
        }
    }

    public static Stack<Tile> GetPath(IntVector2 startPos, IntVector2 targetPos, PathType pType, TileGrid grid)
    {
        var timeBefore = DateTime.Now.Ticks;
        Stack<Tile> path;
        if (pType == PathType.Old) path = OldGetPath(startPos, targetPos);
        else path = AStar.AStarSearch(grid, startPos, targetPos);

        var diff = DateTime.Now.Ticks - timeBefore;
        if (diff != 0)
            UnityEngine.Debug.Log(diff);
        return path;
    }


    public static Stack<Tile> OldGetPath(IntVector2 startPos, IntVector2 targetPos)
    {
        frontier = new Queue<Tile>();
        Tile start = graph[startPos.x, startPos.y];
        Tile target = graph[targetPos.x, targetPos.y];

        foreach (var item in graph)
        {
            item.previousTile = null;
        }

        frontier.Enqueue(start);
        Stack<Tile> path = new Stack<Tile>();

        while (frontier.Count > 0)
        {
            var current = frontier.Dequeue();

            if (current == target)
            {
                while (current != start)
                {
                    path.Push(current);
                    Debug.DrawLine(current.pos, current.previousTile.pos, Color.green, 0.4f);
                    current = current.previousTile;
                }
                return path;
            }
            SearchNeighbors(current);
        }
        return null;
    }

    private static void SearchNeighbors(Tile origin)
    {
        foreach (IntVector2 dir in IntVector2.directions)
        {
            Debug.DrawLine(origin.pos, origin.pos + dir, Color.red, 0.4f);
            SearchNeighbor(origin, dir);
        }
    }

    private static void SearchNeighbor(Tile origin, IntVector2 direction)
    {
        IntVector2 neighborPos = origin.pos + direction;

        if (neighborPos.x < graph.GetLength(0) || neighborPos.x >= 0 ||
            neighborPos.y < graph.GetLength(1) || neighborPos.y >= 0)
        {
            Tile neighbor = graph[neighborPos.x, neighborPos.y];
            if (neighbor.isWalkable)
            {
                if (neighbor.previousTile == null) neighbor.previousTile = origin;
                frontier.Enqueue(neighbor);
            }
        }
    }
}
