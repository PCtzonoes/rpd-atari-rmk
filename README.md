# GameBoy Jam Project - Team 07

Project made for [Atari Remake Jam](home/Placeholder.text) using following constraints:

    Input Constraint  - An Axis and a Button

    Design Constraint - 2D

    Visual Constraint - 160 x 192 pixels, 128 colours max

    Technical Constraint - Default size + 32 mb

---

## Overview

### Description

    Placeholder

### Goals

    Placeholder

### Game Pillars

    Placeholder

---

## References
Placeholder's [Placeholder](home/Placeholder.text)

---